<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'listing', 301);

Route::get('listing','ListingController@index');
Route::get('listing/{productId}','ListingController@show');
Route::post('seller/contact/{productId}','ListingController@contact');



