@include('includes.header')
<div class="card mb-3">
<div class="card-header">
    <i class="fa fa-table"></i> Product Search</div>
<div class="card-body">

        <form action="{{url('listing')}}" method="get">
         {{--{{ csrf_field() }}--}}
            <div class="row">
                <div class="col-xl-3 col-sm-3">
                    <select name="category_id" class="form-control">
                        <option value="">Select Category</option>
                        <?php foreach($categories as $category){?>
                        <option value="{{$category->id}}" <?php if ($category_id==$category->id) {?> selected="selected" <?php }?>>{{$category->title}}</option>
                        <?php }?>
                    </select>
                </div>
                <div class="col-xl-3 col-sm-3">
                    <input class="form-control" id="keyword" name="keyword" type="text"  placeholder="Keyword" value="{{$keyword}}">
                </div>
                <div class="col-xl-2 col-sm-2">
                    <input class="form-control" id="min_price" name="min_price" type="text"  placeholder="Min Price" value="{{$min_price}}">
                </div>
                <div class="col-xl-2 col-sm-2">
                    <input class="form-control" id="max_price" name="max_price" type="text"  placeholder="Max Price" value="{{$max_price}}">
                </div>
                <div class="col-xl-2 col-sm-2">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    <br>
        <hr>
    <div class="row">
        <div id="catlist">
        <?php if ($search) { ?>
            <h3>Search Results (<?=count($products)?>)</h3>

        <?php foreach ($products as $product) {?>

            <dl>
                <dt><img src="{{@$product->productimage->image}}" width="93"  />
                    <a href="{{url('listing/'.$product->id)}}" id="product_title">{{$product->title}}</a></dt>
                <div class="row">
                <div style="width: 50%;float: left">
                    <table>
                        <tr>
                            <td class="font-weight-bold">
                                Year
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{$product->year}}
                            </td>
                            <td class="font-weight-bold">
                                Make
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{$product->model->make->name}}
                            </td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">
                                Model
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{$product->model->name}}
                            </td>
                            <td class="font-weight-bold">
                                Price
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                $ {{number_format($product->price,2)}}
                            </td>
                        </tr>
                    </table>
                </div>
                </div>

            </dl>

        <?php }?>
            <?php }?>
        </div>
    </div>

</div>

</div>
@include('includes.footer')