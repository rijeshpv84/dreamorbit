<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dream Orbit - Test Application</title>
    <!-- Bootstrap core CSS-->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin.css')}}" rel="stylesheet">
    <style>
        #catlist{
            border-bottom:none;
            width:800px;
            margin:10px auto;
        }
        #catlist dl{
            width:800px;
            margin:0 auto;
            border-bottom:1px dashed #ccc;
            padding:10px;
            overflow:hidden;
            background:#f2f2f2;
        }
        #catlist dd{overflow:auto}
        #catlist dt strong{
            float:right;
            padding:0 0 0 20px;
        }
        #catlist dt img{
            float:left;
            margin:0 10px 0 0;
            border:1px solid #000;
        }
    </style>
</head>

<body class="" id="page-top">
<!-- Navigation-->

<div class="content-wrapper">
    <div class="container-fluid">
