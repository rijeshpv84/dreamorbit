@include('includes.header')
<div class="card mb-3">
<div class="card-header">
    <i class="fa fa-table"></i> Product Details

    <a href="javascript:" onclick="window.history.back();" style="float: right">Back</a>

</div>
<div class="card-body">



    <div class="row">
        <div id="catlist">

            <div>
                <?php foreach ($product->productimages as $image){?>
                    <img src="{{$image->image}}" width="300"  />
                <?php }?>
            </div>
            <dl>
                <dt>
                    <h3>{{$product->title}}</h3></dt>
                <div class="row">
                    <div style="width: 50%;float: left;padding-left: 10px">
                        <table>
                            <tr>
                                <td class="font-weight-bold">
                                    Year
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    {{$product->year}}
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">
                                    Make
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    {{$product->model->make->name}}
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">
                                    Model
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    {{$product->model->name}}
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">
                                    Price
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    $ {{number_format($product->price,2)}}
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div style="width: 50%;float: left">
                        <table>
                            <?php foreach($product->attributes as $attribute){?>
                            <tr>
                                <td class="font-weight-bold">
                                    {{$attribute->attributeValues->attribute->title}}
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    {{$attribute->attributeValues->attribute_value}}
                                </td>
                            </tr>
                            <?php }?>
                        </table>
                    </div>
                </div>
                <p> {{$product->description}}</p>
            </dl>

            <div>
                Seller : <?=$product->seller->name?>
            </div>
            <div>
                Rating : <?=$product->seller->rating->rate?>
            </div>
            <br>
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <div class="card-body">
                <form action="{{url('seller/contact/'.$product->id)}}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="exampleInputName">Email</label>
                                <input class="form-control" id="email" name="email" type="text"  placeholder="Enter Email">
                            </div>
                            <div class="col-md-12">
                                <label for="exampleInputName">Mobile</label>
                                <input class="form-control" id="mobile" name="mobile" type="text"  placeholder="Enter Mobile">
                            </div>
                            <div class="col-md-12">
                                <label for="exampleInputName">Message</label>
                                <textarea class="form-control" id="message" name="message" type="text"  placeholder="Enter Message"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Contact Seller</button>
                    </div>
                </form>

            </div>

        </div>
    </div>

</div>

</div>
@include('includes.footer')