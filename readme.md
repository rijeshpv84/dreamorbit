
## Steps to run in local

1. Clone the repository in your local.
**git clone https://rijeshpv84@bitbucket.org/rijeshpv84/dreamorbit.git**

2. Navigate to the project folder
**cd dreamorbit** and then run
**composer install**

3. Create database **orbit** in mysql.

4. Update the database details in **.ini** file.

5. Run migration script for creating all tables.
    **php artisan migrate**
    
6. Run seeder script to load default data and dummy datas. 
   **php artisan db:seed**
   
7. Give permission for the following directory
       **chmod -R 777 storage/ bootstrap/**
         
8. For running the test case run the following command in root.
   **./vendor/bin/phpunit tests/Feature/**
   
9. For viewing the working application navigate to the following url
   **http://localhost/dreamorbit/public/**

