<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    /** @test */
    public function a_buyer_can_see_the_product_search_page()
    {
        $this->visit('listing')
            ->seePageIs('/listing');

    }

    /** @test */
    public function a_buyer_can_visit_search_page_and_submit_search_criteria()
    {
        $this->visit('listing')
            ->select('1', 'category_id')
            ->type('test', 'keyword')
            ->type('10', 'min_price')
            ->type('100', 'max_price')
            ->press('Search')
            ->see('Search Results');
    }

    /** @test */
    public function a_buyer_can_click_product_and_see_details()
    {
        $this->visit('listing')
            ->select('2', 'category_id')
            ->press('Search')
            ->click('product_title')
            ->seePageIs('/listing/1');
    }

    /** @test */
    public function a_buyer_can_see_product_details()
    {
        $this->visit('/listing/1')
            ->see('Product Details');
    }

    /** @test */
    public function a_buyer_can_see_seller_rating()
    {
        $this->visit('/listing/1')
            ->see('Rating');
    }

    /** @test */
    public function a_buyer_can_express_interest_by_submiting_contac_form()
    {
        $this->visit('/listing/1')
            ->type('test', 'email')
            ->type('test', 'mobile')
            ->type('test', 'message')
            ->press('Contact Seller')
            ->see('Message Successfully Sent');
    }
}
