<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    public function rating()
    {
        return $this->hasOne('App\Models\SellerRating');
    }
}
