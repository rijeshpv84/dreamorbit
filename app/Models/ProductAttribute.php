<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    public function attributeValues()
    {
        return $this->belongsTo('App\Models\AttributeValue','attribute_value_id','id');
    }
}
