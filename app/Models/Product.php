<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function productimage()
    {
        return $this->hasOne('App\Models\Productimage');
    }

    public function productimages()
    {
        return $this->hasmany('App\Models\Productimage');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','id');
    }

    public function model()
    {
        return $this->belongsTo('App\Models\Models','model_id','id');
    }

    public function attributes()
    {
        return $this->hasMany('App\Models\ProductAttribute');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller');
    }
}
