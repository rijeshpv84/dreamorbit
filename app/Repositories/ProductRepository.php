<?php namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function search($category, $keyword, $price_min, $price_max)
    {
        $query =  $this->model->with(['productimage', 'category', 'attributes', 'attributes.attributeValues', 'attributes.attributeValues.attribute', 'model', 'model.make']);

        if ($category) {
            $query->where('category_id', $category);
        }

        if (!empty($price_max) && !empty($price_min)){
            $query->whereBetween('price',[$price_min,$price_max]);
        } else if (!empty($price_min)) {
            $query->where('price','>=',$price_min);
        } else if (!empty($price_max)) {
            $query->where('price','<=',$price_max);
        }

        if ($keyword) {
            $query->where(function($query) use ($keyword) {
                $query->whereRaw('MATCH (title,description) AGAINST (?)', array($keyword))
                    ->orWhere('year',$keyword)
                    ->orWhereHas('attributes.attributeValues', function ($query) use ($keyword) {
                        $query->where('attribute_value', 'LIKE', '%' . $keyword . '%');
                    })
                    ->orWhereHas('model', function ($query) use ($keyword) {
                        $query->where('name', 'LIKE', '%' . $keyword . '%');
                    })->orWhereHas('model.make', function ($query) use ($keyword) {
                        $query->where('name', 'LIKE', '%' . $keyword . '%');
                    });
            });
        }


        $res =  $query->get();
        return $res;
    }

    public function getProductDetails($productid)
    {
        $query =  $this->model->with(['productimages', 'category', 'attributes', 'attributes.attributeValues', 'attributes.attributeValues.attribute', 'model', 'model.make', 'seller', 'seller.rating']);
        $query->where('id',$productid);
        $res =  $query->first();
        return $res;
    }
}