<?php namespace App\Repositories;

use App\Models\ProductImage;

class ProductImageRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(ProductImage $productImage)
    {
        $this->model = $productImage;
    }

}