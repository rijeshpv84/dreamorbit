<?php

namespace App\Repositories;

use App\Models\Models;
use Illuminate\Database\Eloquent\Model;

class ModelsRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(Models $models)
    {
        $this->model = $models;
    }
}
