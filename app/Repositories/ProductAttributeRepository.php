<?php

namespace App\Repositories;

use App\Models\ProductAttribute;
use Illuminate\Database\Eloquent\Model;

class ProductAttributeRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(ProductAttribute $productAttribute)
    {
        $this->model = $productAttribute;
    }
}
