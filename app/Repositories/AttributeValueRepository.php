<?php

namespace App\Repositories;

use App\Models\AttributeValue;
use Illuminate\Database\Eloquent\Model;

class AttributeValueRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(AttributeValue $attributeValue)
    {
        $this->model = $attributeValue;
    }
}
