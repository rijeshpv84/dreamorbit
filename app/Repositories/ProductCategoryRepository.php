<?php namespace App\Repositories;

use App\Models\Category;

class ProductCategoryRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(Category $productCategory)
    {
        $this->model = $productCategory;
    }

}