<?php

namespace App\Repositories;

use App\Models\Buyer;
use Illuminate\Database\Eloquent\Model;

class BuyerRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(Buyer $buyer)
    {
        $this->model = $buyer;
    }
}
