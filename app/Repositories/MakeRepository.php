<?php

namespace App\Repositories;

use App\Models\Make;
use Illuminate\Database\Eloquent\Model;

class MakeRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(Make $make)
    {
        $this->model = $make;
    }
}
