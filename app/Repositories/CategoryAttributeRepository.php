<?php

namespace App\Repositories;

use App\Models\CategoryAttribute;
use Illuminate\Database\Eloquent\Model;

class CategoryAttributeRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(CategoryAttribute $categoryAttribute)
    {
        $this->model = $categoryAttribute;
    }
}
