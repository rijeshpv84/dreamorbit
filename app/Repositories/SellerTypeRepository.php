<?php

namespace App\Repositories;

use App\Models\SellerType;
use Illuminate\Database\Eloquent\Model;

class SellerTypeRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(SellerType $sellerType)
    {
        $this->model = $sellerType;
    }
}
