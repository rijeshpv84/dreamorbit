<?php
namespace App\Repositories;

use App\Models\Attribute;

class AttributeRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(Attribute $attribute)
    {
        $this->model = $attribute;
    }
}
