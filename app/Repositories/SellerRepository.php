<?php

namespace App\Repositories;

use App\Models\Seller;
use Illuminate\Database\Eloquent\Model;

class SellerRepository extends AbstractRepository
{
    // Constructor to bind model to repo
    public function __construct(Seller $seller)
    {
        $this->model = $seller;
    }
}
