<?php

namespace App\Http\Controllers;

use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;


class ListingController extends Controller
{
    public function index(ProductRepository $product, ProductCategoryRepository $productCategoryRepository, Request $request)
    {
        $data['categories'] = $productCategoryRepository->getAll();
        $data['products'] = [];
        $data['category_id'] = '';
        $data['keyword'] = '';
        $data['min_price'] = '';
        $data['max_price'] = '';
        $data['search'] = false;
        if ($request->category_id) {
            $data['category_id'] = $request->category_id;
            $data['keyword'] = $request->keyword;
            $data['min_price'] = $request->min_price;
            $data['max_price'] = $request->max_price;
            $data['products'] = $product->search($request->category_id, $request->keyword, $request->min_price, $request->max_price);
            $data['search'] = true;
        }

        return view('listing',$data);
    }

    public function show($productId, ProductRepository $product)
    {
        $data['product'] = $product->getProductDetails($productId);
        return view('listing_details', $data);
    }

    public function contact($productId)
    {
        /*
         *  Validation code will go here
         *  Email code will go here
         * */

        return redirect()->back()->with('message', 'Message Successfully Sent');
    }
}
