<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Seller::class, function (Faker $faker) {
    return [
        'seller_type_id' => '1',
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => '84312'.mt_rand(10000, 99999),
        'email' => $faker->unique()->safeEmail,
        'website' => 'www.'.str_random(10).'.com',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),

    ];
});
