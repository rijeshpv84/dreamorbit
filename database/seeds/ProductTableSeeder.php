<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->insert([
            'seller_id' => 1,
            'category_id' => 2,
            'model_id' => 1,
            'title' => str_random(20),
            'year' => mt_rand(1984,2018),
            'price' => mt_rand(1000,10000),
            'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        \DB::table('products')->insert([
            'seller_id' => 2,
            'category_id' => 2,
            'model_id' => 2,
            'title' => str_random(20),
            'year' => mt_rand(1984,2018),
            'price' => mt_rand(1000,10000),
            'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        \DB::table('products')->insert([
            'seller_id' => 2,
            'category_id' => 2,
            'model_id' => 3,
            'title' => str_random(20),
            'year' => mt_rand(1984,2018),
            'price' => mt_rand(1000,10000),
            'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        \DB::table('products')->insert([
            'seller_id' => 2,
            'category_id' => 2,
            'model_id' => 2,
            'title' => str_random(20),
            'year' => mt_rand(1984,2018),
            'price' => mt_rand(1000,10000),
            'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        \DB::table('products')->insert([
            'seller_id' => 1,
            'category_id' => 2,
            'model_id' => 5,
            'title' => str_random(20),
            'year' => mt_rand(1984,2018),
            'price' => mt_rand(1000,10000),
            'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $products = \App\Models\Product::get();

        $carImages = [
            'https://auto.ndtvimg.com/car-images/big/maruti-suzuki/baleno/maruti-suzuki-baleno.jpg?v=51',
            'https://imgd.aeplcdn.com/1280x720/cw/ec/26523/Maruti-Suzuki-Ciaz-Facelift-Exterior-87489.jpg?wm=0&q=100',
            'https://imgct2.aeplcdn.com/img/800x600/car-DATA/big/maruti-suzuki-swift-image-11306.png?v=26',
            'https://images.pexels.com/photos/170811/pexels-photo-170811.jpeg?auto=compress&cs=tinysrgb&h=350',
            'https://images.pexels.com/photos/120049/pexels-photo-120049.jpeg?auto=compress&cs=tinysrgb&h=350',
            'https://media.zigcdn.com/media/photogallery/2018/Feb/akjdha.jpg',
            'https://images.pexels.com/photos/358070/pexels-photo-358070.jpeg?auto=compress&cs=tinysrgb&h=350',
            'https://images.pexels.com/photos/50704/car-race-ferrari-racing-car-pirelli-50704.jpeg?auto=compress&cs=tinysrgb&h=350'
        ];

        foreach ($products as $val) {
            DB::table('product_images')->insert([
                'product_id' => $val->id,
                'image' => $carImages[array_rand($carImages)],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            DB::table('product_images')->insert([
                'product_id' => $val->id,
                'image' => $carImages[array_rand($carImages)],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            DB::table('product_attributes')->insert([
                'product_id' => $val->id,
                'attribute_value_id' => rand(1,10),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            DB::table('product_attributes')->insert([
                'product_id' => $val->id,
                'attribute_value_id' => rand(1,10),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }



    }
}
