<?php

use Illuminate\Database\Seeder;

class SellersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Models\Seller::class, 5)->create();

        $sellers = \App\Models\Seller::get();

        foreach ($sellers as $val) {
            DB::table('seller_ratings')->insert([
                'seller_id' => $val->id,
                'rate' => mt_rand(1, 5),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

    }
}
