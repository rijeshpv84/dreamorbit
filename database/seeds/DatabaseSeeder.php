<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(SellerTypesTableSeeder::class);
        $this->call(SellersTableSeeder::class);
        $this->call(MakeTableSeeder::class);
        $this->call(ModelTableSeeder::class);
        $this->call(AttributeTableSeeder::class);
        $this->call(AttributeValuesTableSeeder::class);

        $this->call(ProductTableSeeder::class);

    }
}
