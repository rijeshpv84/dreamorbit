<?php

use Illuminate\Database\Seeder;

class ModelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $make = \App\Models\Make::where(['name'=>'AUDI'])->first();
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => '	A1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => '	A3',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => '	Q2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);



        $make = \App\Models\Make::where(['name'=>'BMW'])->first();
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => '1 Series',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => '2 Series',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => '	X1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $make = \App\Models\Make::where(['name' => 'FORD'])->first();
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => 'B-Max',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => 'C-MAX',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => 'Fiesta',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);


        $make = \App\Models\Make::where(['name' => 'HYUNDAI'])->first();
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => 'i10',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => 'i20',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('models')->insert([
            'make_id' => $make->id,
            'name' => 'Tucson',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

    }
}
