<?php

use Illuminate\Database\Seeder;

class AttributeValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Color
        \DB::table('attribute_values')->insert([
            'attribute_id' => '1',
            'attribute_value' => 'Red',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('attribute_values')->insert([
            'attribute_id' => '1',
            'attribute_value' => 'White',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('attribute_values')->insert([
            'attribute_id' => '1',
            'attribute_value' => 'Blue',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('attribute_values')->insert([
            'attribute_id' => '1',
            'attribute_value' => 'Black',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        //Transmission
        \DB::table('attribute_values')->insert([
            'attribute_id' => '2',
            'attribute_value' => '4',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('attribute_values')->insert([
            'attribute_id' => '2',
            'attribute_value' => '5',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('attribute_values')->insert([
            'attribute_id' => '2',
            'attribute_value' => '6',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('attribute_values')->insert([
            'attribute_id' => '2',
            'attribute_value' => 'Auto',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        //Body Type
        \DB::table('attribute_values')->insert([
            'attribute_id' => '3',
            'attribute_value' => 'Sedan',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \DB::table('attribute_values')->insert([
            'attribute_id' => '3',
            'attribute_value' => 'Hatch Back',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

    }
}
